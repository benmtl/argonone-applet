# Changelog

Version 0.0.1 -- Initial Release  

Version 0.0.2 -- UI Tweaking  

- Minor Graphical change the the argonone-fan-off change the slash to black to match the other icons.
- Remake the argonone-fan-error icon to keep to the style of the other icons.
- Added Temperature read out option.
- Changed Tooltip to give information about mode and fan status.
- Reworked some string handling.  

Version 0.0.3 --  

- When restarting read the configuration for applet
- Fixed warning: embedded ‘\0’
- Updated documentation

Version 0.0.4 -- Minor bug patch

- FIX: memory leak in getstate()
- FIX: Formatting issues in README.md and CHANGELOG.md
- ADD: About box to menu
